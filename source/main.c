#include <stdio.h>
#include <stdlib.h>
#include <gccore.h>
#include <ogc/gu.h>
#include <ogc/lwp_watchdog.h>
#include <wiiuse/wpad.h>

static void *xfb = NULL;
static GXRModeObj *rmode = NULL;

static float elapsed_ms(u64 time0, u64 time1)
{
    return (time1 - time0) / (float)TB_TIMER_CLOCK;
}

void run_benchmark()
{
    u64 time0, time1;
    int count = 10000;

    Mtx44 a_orig = {
        { 1.2, -2.3, 3.4, 4.5 },
        { 5.6, 6.7, 7.8, -8.9 },
        { -2.1, 3.2, -4.3, 5.4 },
        { 6.5, 7.6, 8.7, -9.8 },
    };

    Mtx44 b_orig = {
        { 0.1, 0.2, -0.3, 1.4 },
        { -0.4, 0.3, 0.2, 0.1 },
        { 0.01, 1.1, -0.1, 0.4 },
        { 0.04, -0.2, -0.1, 1.1 },
    };
    Mtx44 a, b;
    guMtx44Copy(a_orig, a);
    guMtx44Copy(b_orig, b);

    Mtx44 rc;
    Mtx44 rps;

    time0 = gettime();
    for (int i = 0; i < count; i++) {
        c_guMtx44Concat(a_orig, b, a);
    }
    time1 = gettime();
    printf("Ran c_guMtxConcat %d times, time: %.3fms\n", count, elapsed_ms(time0, time1));
    guMtx44Copy(a, rc);

    guMtx44Copy(a_orig, a);
    guMtx44Copy(b_orig, b);
    time0 = gettime();
    for (int i = 0; i < count; i++) {
        ps_guMtx44Concat(a_orig, b, a);
    }
    time1 = gettime();
    printf("Ran ps_guMtxConcat %d times, time: %.3fms\n", count, elapsed_ms(time0, time1));
    guMtx44Copy(a, rps);

    printf("   Result (C)             Result (PS)\n");
    for (int row = 0; row < 4; row ++) {
        printf("%5.2f %5.2f %5.2f %5.2f    %5.2f %5.2f %5.2f %5.2f\n",
               rc[row][0], rc[row][1], rc[row][2], rc[row][3],
               rps[row][0], rps[row][1], rps[row][2], rps[row][3]);
    }
}

//---------------------------------------------------------------------------------
int main(int argc, char **argv) {
//---------------------------------------------------------------------------------

	// Initialise the video system
	VIDEO_Init();

	// This function initialises the attached controllers
	WPAD_Init();

	// Obtain the preferred video mode from the system
	// This will correspond to the settings in the Wii menu
	rmode = VIDEO_GetPreferredMode(NULL);

	// Allocate memory for the display in the uncached region
	xfb = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));

	// Initialise the console, required for printf
	console_init(xfb,20,20,rmode->fbWidth,rmode->xfbHeight,rmode->fbWidth*VI_DISPLAY_PIX_SZ);

	// Set up the video registers with the chosen mode
	VIDEO_Configure(rmode);

	// Tell the video hardware where our display memory is
	VIDEO_SetNextFramebuffer(xfb);

	// Make the display visible
	VIDEO_SetBlack(false);

	// Flush the video register changes to the hardware
	VIDEO_Flush();

	// Wait for Video setup to complete
	VIDEO_WaitVSync();
	if(rmode->viTVMode&VI_NON_INTERLACE) VIDEO_WaitVSync();


	// The console understands VT terminal escape codes
	// This positions the cursor on row 2, column 0
	// we can use variables for this with format codes too
	// e.g. printf ("\x1b[%d;%dH", row, column );
	printf("\x1b[2;0H");


	printf("Hello World!\n");

	while(1) {

		// Call WPAD_ScanPads each loop, this reads the latest controller states
		WPAD_ScanPads();

		// WPAD_ButtonsDown tells us which buttons were pressed in this loop
		// this is a "one shot" state which will not fire again until the button has been released
		u32 pressed = WPAD_ButtonsDown(0);

		// We return to the launcher application via exit
		if ( pressed & WPAD_BUTTON_HOME ) exit(0);

		if ( pressed & WPAD_BUTTON_A ) run_benchmark();

		// Wait for the next frame
		VIDEO_WaitVSync();
	}

	return 0;
}
